using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrudModel.Models
{
    public class Employee
    {

        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

    }
}